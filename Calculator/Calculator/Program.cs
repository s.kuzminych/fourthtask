﻿using System;

namespace Calculator
{
    class Program
    {
        static string previousCalculations = "";
        static double? lastOperationResult = null;

        static void Main(string[] args)
        {
            Console.WriteLine("================================= Calculator =================================\n");
            Operate();
            ExitApp();
        }

        static void Operate()
        {
            Console.WriteLine("Please, enter a command. Type \"h\" to see all commands.");
            string input = Console.ReadLine();
            switch (input)
            {
                case "+":
                case "add":
                    lastOperationResult = Add();
                    break;
                case "-":
                case "sub":
                    lastOperationResult = Sub();
                    break;
                case "*":
                case "mp":
                    lastOperationResult = Multiplication();
                    break;
                case "/":
                case "div":
                    lastOperationResult = Div();
                    break;
                case "mm":
                case "matrix mp":
                    MultiplicateMatrix();
                    lastOperationResult = null;
                    break;
                case "c":
                case "clear":
                    Clear();
                    break;
                case "e":
                case "exit":
                    ExitApp();
                    break;
                case "h":
                case "help":
                    ShowHelp();
                    break;
                case "m":
                case "memory":
                    if (previousCalculations == "")
                    {
                        Console.WriteLine("There is no previous calculations.\n");
                    }
                    else
                    {
                        Console.WriteLine("Previous calculations:\n");
                        Console.WriteLine(previousCalculations);
                    }
                    break;
                case "mc":
                case "memory clear":
                    previousCalculations = "";
                    Console.WriteLine("Memory cleared.\n");
                    break;
                default:
                    Console.WriteLine("\nIncorrect input. Please, try again.\n");
                    break;
            }
            Operate();
        }

        static void ExitApp()
        {
            Console.WriteLine("\nThank you for using this application.\n");
            Console.WriteLine("Siarhei.Kuzminykh@itechart-group.com - 2019");
            System.Threading.Thread.Sleep(2000);
            Environment.Exit(0);
        }

        static double ReadAndConvertToDouble(string userMessage, bool canUsePreviousResult)
        {
            if ((canUsePreviousResult) && (lastOperationResult != null))
            {
                Console.WriteLine(userMessage + " Press Enter to use result of the previous operation: " + lastOperationResult.ToString() + ".");
            }
            else
            {
                Console.WriteLine(userMessage);
            }
            string input = Console.ReadLine();
            double result = 0;
            switch (input)
            {
                case "":
                    if ((lastOperationResult == null) || (!canUsePreviousResult))
                    {
                        Console.WriteLine("This is not a valid number. Please, try again.");
                        result = ReadAndConvertToDouble(userMessage, false);
                    }
                    else
                    {
                        Console.WriteLine(lastOperationResult.ToString());
                        result = (double)lastOperationResult;
                    }
                    break;
                case "h":
                case "help":
                    ShowHelp();
                    result = ReadAndConvertToDouble(userMessage, canUsePreviousResult);
                    break;
                case "e":
                case "exit":
                    ExitApp();
                    break;
                case "c":
                case "clear":
                    Clear();
                    break;
                default:
                    if ((!double.TryParse(input, out result)) || (input.Contains(",")))
                    {
                        Console.WriteLine("This is not a valid number. Please, try again.");
                        result = ReadAndConvertToDouble(userMessage, canUsePreviousResult);
                    }
                    break;
            }
            return result;
        }

        static void ShowHelp()
        {
            Console.WriteLine();
            Console.WriteLine("This application is a calculator with 4 basic arithmetic operations and matrix multiplication.");
            Console.WriteLine();
            Console.WriteLine("Arithmetic operations commands:");
            Console.WriteLine();
            Console.WriteLine("    Enter \"+\" or \"add\" to do addition.");
            Console.WriteLine("    Enter \"-\" or \"sub\" to do subtraction.");
            Console.WriteLine("    Enter \"*\" or \"mp\" to do multiplication.");
            Console.WriteLine("    Enter \"/\" or \"div\" to do division.");
            Console.WriteLine("    Enter \"mm\" or \"matrix mp\" to do matrix multiplication.");
            Console.WriteLine();
            Console.WriteLine("Auxiliary operations:");
            Console.WriteLine();
            Console.WriteLine("    Enter \"c\" or \"clear\" to clear the calculation buffer. You can enter this command at any time.");
            Console.WriteLine("    Enter \"e\" or \"exit\" to exit the application. You can enter this command at any time.");
            Console.WriteLine("    Enter \"h\" or \"help\" to see this message again. You can enter this command at any time.");
            Console.WriteLine("    Enter \"m\" or \"memory\" to see the history of previous calculations.");
            Console.WriteLine("    Enter \"mc\" or \"memory clear\" to clear the history of previous calculations.");
            Console.WriteLine();
        }

        static void Clear()
        {
            Console.WriteLine("The result of the previous operation is cleared.\n");
            lastOperationResult = null;
            Operate();
            ExitApp();
        }

        static double Add()
        {
            double firstAddend = ReadAndConvertToDouble("Please, enter first addend.", true);
            double secondAddend = ReadAndConvertToDouble("Please, enter second addend.", false);
            double result = firstAddend + secondAddend;
            previousCalculations = previousCalculations + firstAddend.ToString() + "+" + secondAddend.ToString() + "=" + result.ToString() + "\n";
            Console.WriteLine(firstAddend.ToString() + "+" + secondAddend.ToString() + "=" + result.ToString());
            Console.WriteLine("Result is: " + result.ToString() + ".\n");
            return result;
        }

        static double Sub()
        {
            double minuend = ReadAndConvertToDouble("Please, enter minuend.", true);
            double subtrahend = ReadAndConvertToDouble("Please, enter subtrahend.", false);
            double result = minuend - subtrahend;
            previousCalculations = previousCalculations + minuend.ToString() + "-" + subtrahend.ToString() + "=" + result.ToString() + "\n";
            Console.WriteLine(minuend.ToString() + "-" + subtrahend.ToString() + "=" + result.ToString());
            Console.WriteLine("Result is: " + result.ToString() + ".\n");
            return result;
        }

        static double Multiplication()
        {
            double firstFactor = ReadAndConvertToDouble("Please, enter first factor.", true);
            double secondFactor = ReadAndConvertToDouble("Please, enter second factor.", false);
            if (firstFactor*secondFactor > double.MaxValue)
            {
                Console.WriteLine("The result is greater than acceptable values.");
                Clear();
            }
            if (firstFactor * secondFactor < double.MinValue)
            {
                Console.WriteLine("The result is less than acceptable values.");
                Clear();
            }
            double result = firstFactor * secondFactor;
            previousCalculations = previousCalculations + firstFactor.ToString() + "x" + secondFactor.ToString() + "=" + result.ToString() + "\n";
            Console.WriteLine(firstFactor.ToString() + "x" + secondFactor.ToString() + "=" + result.ToString());
            Console.WriteLine("Result is: " + result.ToString() + ".\n");
            return result;
        }

        static double Div()
        {
            double dividend = ReadAndConvertToDouble("Please, enter dividend.", true);
            double divisor;
            while (true)
            {
                divisor = ReadAndConvertToDouble("Please, enter divisor.", false);
                if (divisor != 0)
                {
                    break;
                }
                Console.WriteLine("Zero can not be a divisor. Please, try again.");
            }
            double result = dividend / divisor;
            previousCalculations = previousCalculations + dividend.ToString() + "/" + divisor.ToString() + "=" + result.ToString() + "\n";
            Console.WriteLine(dividend.ToString() + "/" + divisor.ToString() + "=" + result.ToString());
            Console.WriteLine("Result is: " + result.ToString() + ".\n");
            return result;
        }

        static void MultiplicateMatrix()
        {
            byte firstMatrixColCount = ReadAndConvertMatrixParameter("Please, enter the number of columns of the first matrix. It must be an integer in [1;9]");
            byte firstMatrixRowCount = ReadAndConvertMatrixParameter("Please, enter the number of rows of the first matrix. It must be an integer in [1;9]");
            byte secondMatrixColCount = ReadAndConvertMatrixParameter("Please, enter the number of columns of the second matrix. It must be an integer in [1;9]");
            byte secondMatrixRowCount = ReadAndConvertMatrixParameter("Please, enter the number of rows of the second matrix. It must be an integer in [1;9]");
            if (firstMatrixColCount != secondMatrixRowCount)
            {
                Console.WriteLine("These matrices cannot be multiplied. The number of columns on matrix A is not the same as the number of rows of matrix B. Please, try again.");
                MultiplicateMatrix();
                return;
            }
            Console.WriteLine("Matrix representation:");
            Console.WriteLine(" __" + new string(' ', ((5 * firstMatrixColCount) - 2)) + "__     __" + new string(' ', ((5 * secondMatrixColCount) - 2)) + "__");
            Console.WriteLine("|" + new string(' ', (5 * firstMatrixColCount)) + "  |   |" + new string(' ', (5 * secondMatrixColCount) - 1) + "   |");
            byte maxRowCount = secondMatrixRowCount;
            if (firstMatrixRowCount > secondMatrixRowCount)
            {
                maxRowCount = firstMatrixRowCount;
            }
            int middleRow = maxRowCount / 2;
            for (int i = 0; i < maxRowCount; i++)
            {
                Console.Write("|");
                for (int j = 0; j < firstMatrixColCount; j++)
                {
                    if (i >= firstMatrixRowCount)
                    {
                        Console.Write("     ");
                    }
                    else
                    {
                        Console.Write("  a" + (i + 1).ToString() + (j + 1).ToString());
                    }
                }
                Console.Write("  |");
                if (middleRow == i)
                {
                    Console.Write(" x |");
                }
                else
                {
                    Console.Write("   |");
                }

                for (int j = 0; j < secondMatrixColCount; j++)
                {
                    if (i >= secondMatrixRowCount)
                    {
                        Console.Write("     ");
                    }
                    else
                    {
                        Console.Write("  b" + (i + 1).ToString() + (j + 1).ToString());
                    }
                }
                Console.WriteLine("  |");
            }
            Console.WriteLine("|__" + new string(' ', ((5 * firstMatrixColCount) - 2)) + "__|   |__" + new string(' ', ((5 * secondMatrixColCount) - 2)) + "__|");
            double[,] firstMatrix = new double[firstMatrixRowCount, firstMatrixColCount];
            double[,] secondMatrix = new double[secondMatrixRowCount, secondMatrixColCount];
            Console.WriteLine("First matrix:");
            for (int i = 0; i < firstMatrixRowCount; i++)
            {
                for (int j = 0; j < firstMatrixColCount; j++)
                {
                    firstMatrix[i, j] = ReadAndConvertMatrixElement("Enter a" + (i + 1).ToString() + (j + 1).ToString() + ": ");
                }
            }
            Console.WriteLine("Second matrix:");
            for (int i = 0; i < secondMatrixRowCount; i++)
            {
                for (int j = 0; j < secondMatrixColCount; j++)
                {
                    secondMatrix[i, j] = ReadAndConvertMatrixElement("Enter b" + (i + 1).ToString() + (j + 1).ToString() + ": ");
                }
            }
            double[,] resultMatrix = Multiplicate(firstMatrix, secondMatrix);
            int resultMatrixColCount = resultMatrix.GetLength(1);
            int resultMatrixRowCount = resultMatrix.GetLength(0);

            Console.WriteLine("Matrix representation:");
            Console.WriteLine(" __" + new string(' ', ((5 * firstMatrixColCount) - 2)) + "__     __" + new string(' ', ((5 * secondMatrixColCount) - 2)) + "__     __" + new string(' ', ((5 * resultMatrix.GetLength(1)) - 2)) + "__");
            previousCalculations += " __" + new string(' ', ((5 * firstMatrixColCount) - 2)) + "__     __" + new string(' ', ((5 * secondMatrixColCount) - 2)) + "__     __" + new string(' ', ((5 * resultMatrixColCount) - 2)) + "__\n";
            Console.WriteLine("|" + new string(' ', (5 * firstMatrixColCount)) + "  |   |" + new string(' ', (5 * secondMatrixColCount) - 1) + "   |   |" + new string(' ', (5 * resultMatrixColCount) - 1) + "   |");
            previousCalculations += "|" + new string(' ', (5 * firstMatrixColCount)) + "  |   |" + new string(' ', (5 * secondMatrixColCount) - 1) + "   |   |" + new string(' ', (5 * resultMatrixColCount) - 1) + "   |\n";
            for (int i = 0; i < maxRowCount; i++)
            {
                Console.Write("|");
                previousCalculations += "|";
                for (int j = 0; j < firstMatrixColCount; j++)
                {
                    if (i >= firstMatrixRowCount)
                    {
                        Console.Write("     ");
                        previousCalculations += "     ";
                    }
                    else
                    {
                        Console.Write("{0,5}", firstMatrix[i, j].ToString());
                        previousCalculations += string.Format("{0,5}", firstMatrix[i, j].ToString());
                    }
                }
                Console.Write("  |");
                previousCalculations += "  |";
                if (middleRow == i)
                {
                    Console.Write(" x |");
                    previousCalculations += " x |";
                }
                else
                {
                    Console.Write("   |");
                    previousCalculations += "   |";
                }

                for (int j = 0; j < secondMatrixColCount; j++)
                {
                    if (i >= secondMatrixRowCount)
                    {
                        Console.Write("     ");
                        previousCalculations += "     ";
                    }
                    else
                    {
                        Console.Write("{0,5}", secondMatrix[i, j].ToString());
                        previousCalculations += string.Format("{0,5}", secondMatrix[i, j].ToString());
                    }
                }
                Console.Write("  |");
                previousCalculations += "  |";
                if (middleRow == i)
                {
                    Console.Write(" = |");
                    previousCalculations += " = |";
                }
                else
                {
                    Console.Write("   |");
                    previousCalculations += "   |";
                }
                for (int j = 0; j < resultMatrixColCount; j++)
                {
                    if (i >= resultMatrixRowCount)
                    {
                        Console.Write("     ");
                        previousCalculations += "     ";
                    }
                    else
                    {
                        Console.Write("{0,5}", resultMatrix[i, j].ToString());
                        previousCalculations += string.Format("{0,5}", resultMatrix[i, j].ToString());
                    }
                }
                Console.WriteLine("  |");
                previousCalculations += "  |\n";
            }
            Console.WriteLine("|__" + new string(' ', ((5 * firstMatrixColCount) - 2)) + "__|   |__" + new string(' ', ((5 * secondMatrixColCount) - 2)) + "__|   |__" + new string(' ', ((5 * resultMatrixColCount) - 2)) + "__|");
            previousCalculations += "|__" + new string(' ', ((5 * firstMatrixColCount) - 2)) + "__|   |__" + new string(' ', ((5 * secondMatrixColCount) - 2)) + "__|   |__" + new string(' ', ((5 * resultMatrixColCount) - 2)) + "__|\n";
            Console.WriteLine("Result is:");
            Console.WriteLine(" __" + new string(' ', ((21 * resultMatrixColCount) - 2)) + "__");
            Console.WriteLine("|" + new string(' ', (21 * resultMatrixColCount) - 1) + "   |");
            for (int i = 0; i < resultMatrixRowCount; i++)
            {
                Console.Write("|");
                for (int j = 0; j < resultMatrixColCount; j++)
                {
                    Console.Write("{0,21}", resultMatrix[i, j].ToString());
                }
                Console.WriteLine("  |");
            }
            Console.WriteLine("|__" + new string(' ', ((21 * resultMatrixColCount) - 2)) + "__|");
        }

        static double[,] Multiplicate(double[,] a, double[,] b)
        {
            double[,] c = new double[a.GetLength(0), b.GetLength(1)];
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < b.GetLength(1); j++)
                {
                    for (int k = 0; k < a.GetLength(1); k++)
                    {
                        c[i, j] += a[i, k] * b[k, j];
                    }
                }
            }
            return c;
        }
        static double ReadAndConvertMatrixElement(string userMessage)
        {
            Console.WriteLine(userMessage);
            string input = Console.ReadLine();
            double result = 0;
            switch (input)
            {
                case "h":
                case "help":
                    ShowHelp();
                    result = ReadAndConvertMatrixElement(userMessage);
                    break;
                case "e":
                case "exit":
                    ExitApp();
                    break;
                case "c":
                case "clear":
                    Clear();
                    break;
                default:
                    if ((!double.TryParse(input, out result))||(input.Contains(",")))
                    {
                        Console.WriteLine("This is not a valid number. Please, try again.");
                        result = ReadAndConvertMatrixElement(userMessage);
                    }
                    break;
            }
            return result;
        }

        static byte ReadAndConvertMatrixParameter(string userMessage)
        {
            Console.WriteLine(userMessage);
            string input = Console.ReadLine();
            byte result = 0;
            switch (input)
            {
                case "h":
                case "help":
                    ShowHelp();
                    result = ReadAndConvertMatrixParameter(userMessage);
                    break;
                case "e":
                case "exit":
                    ExitApp();
                    break;
                case "c":
                case "clear":
                    Clear();
                    break;
                default:
                    if ((!byte.TryParse(input, out result)) || (input.Contains(",")))
                    {
                        Console.WriteLine("This parameter is invalid. Please, try again.");
                        result = ReadAndConvertMatrixParameter(userMessage);
                    }
                    else if ((result < 0) || (result > 9))
                    {
                        Console.WriteLine("This parameter must be in [1;9]. Please, try again.");
                        result = ReadAndConvertMatrixParameter(userMessage);
                    }
                    break;
            }
            return result;
        }
    }
}
